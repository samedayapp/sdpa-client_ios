//
//  AppDelegate.h
//  SDPAClient
//
//  Created by Sapana Ranipa on 23/12/15.
//  Copyright © 2015 Elluminati. All rights reserved.

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "MBProgressHUD.h"
#import <CoreLocation/CoreLocation.h>

//#define StripePublishableKey @"pk_test_SO6i7bqo2aIODm6HX4HYKvkb"
#define StripePublishableKey @"pk_live_SI0QNV1vnwDRCubysjg7dZqe"

extern NSString *device_token;
extern NSString *device_type;
extern int pushId;
extern NSMutableArray *arrForState;

@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate>
{
    MBProgressHUD *HUD;
    UIView *viewLoading;
    CLLocationManager *locationManager;
}
@property (strong, nonatomic) UIWindow *window;
+(AppDelegate *)sharedAppDelegate;
- (BOOL)connected;
- (NSString *)applicationCacheDirectoryString;
-(void) showHUDLoadingView:(NSString *)strTitle;
-(void) hideHUDLoadingView;
-(void)showToastMessage:(NSString *)message;
-(void)showToastMessageCenter:(NSString *)message;

-(void)showLoadingWithTitle:(NSString *)title;
-(void)hideLoadingView;
@end

