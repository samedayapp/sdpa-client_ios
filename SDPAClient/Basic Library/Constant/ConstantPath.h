//
//  ConstantPath.h
//  SDPAClient
//
//  Created by Sapana Ranipa on 23/12/15.
//  Copyright © 2015 Sapana Ranipa. All rights reserved.

#ifndef ConstantPath_h
#define ConstantPath_h

#ifdef DEBUG
#   define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#   define DLog(...)
#endif
#define APPDELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define Card_Length 19
#define Card_Month 2
#define Card_Year 2
#define Card_CVC_CVV 4

// Googleapis URL
#define Address_URL @"https://maps.googleapis.com/maps/api/geocode/json?"
#define AutoComplete_URL @"https://maps.googleapis.com/maps/api/place/autocomplete/json?"

//Googleapis Keys
//#define Google_Map_Key @"AIzaSyA7qLKZvifMi-xxWwxXv2XckWBk-UZjnWI"
//#define GOOGLE_KEY @"AIzaSyC3VHvOu3t7ToVBGcvTg3jkGTFiYeHBxAc"

#define Google_Map_Key @"AIzaSyB0mWs4IWR6NQp7pLLo-mu32H0JA_4NYxE"
#define GOOGLE_KEY @"AIzaSyB0mWs4IWR6NQp7pLLo-mu32H0JA_4NYxE"

// Base URL

#define API_URL @"http://52.24.145.28/user/"
#define SERVICE_URL @"http://52.24.145.28/"
#define PRIVACY_URL @"http://samedaypickupautotransport.com/legal/t-and-c-client/"
#define HELP_URL @"http://samedaypickupautotransport.com/contact/"

#define ACCEPTABLE_NUMBERS @"1234567890"

// PathName For WebServices

#define P_LOGIN @"login"
#define P_FORGET_PASSWORD @"app/forget_password"
#define P_GET_STATE @"app/get_state"
#define P_GET_MAKES @"app/get_model"
#define P_REGISTER @"register"
#define P_OTP @"confirm_otp"
#define P_GET_ALERT @"app/get_alert"
#define P_REFERRAL @"referral"
#define P_CREATE_REQUEST @"createrequest"
#define P_ADD_CARD @"addcardtoken"
#define P_GET_CARDS @"cards"
#define P_SET_DEFAULT_CARD @"selectcard"
#define P_REMOVE_CARD @"deletecardtoken"
#define P_GET_REQUEST @"get_request"
#define P_GET_REQUEST_PROGRESS @"requestinprogress"
#define P_CANCEL_REQUEST @"cancel_request"
#define P_DELETE_REQUEST @"delete_request"
#define P_INCOMING_BID_REQUEST @"incoming_bidding_requests"
#define P_RESPONSE_TO_REQUEST @"respond_request"
#define P_CONFIRM_TRIP @"request_complete_confirm"
#define P_EXPIRY_REQUEST @"expiryupdate"
#define P_FEEDBACK @"rating"
#define P_UPDATE_PROFILE @"update"
#define P_GET_HISTORY @"get_history"
#define P_LOGOUT @"logout"
#define P_YEAR @"year"
#define P_MAKE @"make"
#define P_MODEL @"model"

// SegueName For Navigation

#define LOGIN_SUCCESS @"segueToLoginSuccess"
#define DIRECT_LOGIN @"SegueToDirectLogin"
#define REGISTER_SUCCESS @"SegueToSuccessRegister"
#define REFERRAL_SUCCESS @"segueToReferralSuccess"
#define SEGUE_TO_PAYMENT @"segueToPaymentVC"
#define SEGUE_TO_PROFILE @"segueToProfileVC"
#define SEGUE_TO_HELP @"segueToHelpVC"
#define SEGUE_TO_HISTORY @"segueToHistoryVC"
#define SEGUE_TO_BID @"segueToBidVC"
#define SEGUE_TO_BID_SUCCESS @"segueToBidSuccess"
#define SEGUE_TO_FEEDBACK @"segueToFeedBack"
#define SEGUE_TO_UNWIND @"SegueToHome"
#define SEGUE_TO_TERMS @"segueToTerms"


#endif