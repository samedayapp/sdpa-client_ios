//
//  ConstantParam.h
//  SDPAClient
//
//  Created by Sapana Ranipa on 23/12/15.
//  Copyright © 2015 Sapana Ranipa. All rights reserved.
//

#ifndef ConstantParam_h
#define ConstantParam_h

// define all require ParamName

#define PARAM_DEVICE_TYPE @"device_type"
#define PARAM_DEVICE_TOKEN @"device_token"
#define PARAM_PICTURE @"picture"
#define PARAM_NAME @"name"
#define PARAM_USERNAME @"username"
#define PARAM_EMAIL @"email"
#define PARAM_USERNAME @"username"
#define PARAM_PHONE @"phone"
#define PARAM_OTP @"otp"
#define PARAM_PASSWORD @"password"
#define PARAM_TYPE @"type"
#define PARAM_CODE @"code"
#define PARAM_TOKEN @"token"
#define PARAM_ID @"id"
#define PARAM_ADDRESS @"address"
#define PARAM_KEY @"key"
#define PARAM_DESCRIPTION @"description"
#define PARAM_SOURCE_LATITUDE @"src_latitude"
#define PARAM_SOURCE_LONGITUDE @"src_longitude"
#define PARAM_DESTINATION_LATITUDE @"dest_latitude"
#define PARAM_DESTINATION_LONGITUDE @"dest_longitude"
#define PARAM_SOURCE_ADDRESS @"src_address"
#define PARAM_DESTINATION_ADDRESS @"dest_address"
#define PARAM_STATE_ID @"state_id"
#define PARAM_MAKE @"make"
#define PARAM_MODEL @"model"
#define PARAM_YEAR @"year"
#define PARAM_TRIM @"trim"
#define PARAM_EXPIRE_TIME @"exp_time"
#define PARAM_OPERATIONAL @"is_operational"
#define PARAM_TRANSPORT @"type_transport"
#define PARAM_LAST_FOUR @"last_four"
#define PARAM_STRIPE_TOKEN @"payment_token"
#define PARAM_CARD_ID @"card_id"
#define PARAM_REQUEST_ID @"request_id"
#define PARAM_ACCEPT @"accepted"
#define PARAM_BID_ID @"bidding_id"
#define PARAM_RATING @"rating"
#define PARAM_COMMENT @"comment"
#define PARAM_OLD_PASSWORD @"old_password"
#define PARAM_NEW_PASSWORD @"new_password"

// define all require PreferenceName
#define PREF_DEVICE_TOKEN @"device_token"
#define PREF_IS_LOGIN @"is_login"
#define PREF_LOGIN_OBJECT @"login_object"
#define PREF_TOKEN @"token"
#define PREF_ID @"id"
#define PREF_REQUEST_ID @"request_id"
#define PREF_REQUEST_DESCRIPTION @"description"
#define PREF_SOURCE_ADDRESS @"src_address"
#define PREF_DESTINATION_ADDRESS @"dest_address"
#define PREF_VEHICLE_YEAR @"year"
#define PREF_VEHICLE_MODEL @"model"
#define PREF_VEHICLE_MAKE @"make"
#define PREF_VEHICLE_TRIM @"trim"
#define PREF_EXPIRY_DATE @"exp_time"
#define PREF_DOWNPAYMENT @"downpayment"
#define PREF_BID_PRICE @"bid_price"
#define PREF_REQUEST_ACCEPT @"accept"
#define PREF_DEFAULT_CARD @"default_card"
#define PREF_IS_RUNNING @"is_operational"
#define PREF_IS_TRANSPORT @"type_transport"

#endif /* ConstantParam_h */
