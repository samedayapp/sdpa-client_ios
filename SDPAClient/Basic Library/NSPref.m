//
//  NSPref.m
//  SDPAClient
//
//  Created by Sapana Ranipa on 23/12/15.
//  Copyright © 2015 Sapana Ranipa. All rights reserved.
//

#import "NSPref.h"

@implementation NSPref
+(void)SetPreference:(NSString *)key Value:(id)value
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    [pref setObject:value forKey:key];
    [pref synchronize];
}
+(id)GetPreference:(NSString *)key
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    return [pref valueForKey:key];
}
+(void)RemovePreference:(NSString *)key
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    [pref removeObjectForKey:key];
    [pref synchronize];
}
+(void)SetBoolPreference:(NSString *)key Value:(BOOL)value
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    [pref setBool:value forKey:key];
    [pref synchronize];
}
+(BOOL)GetBoolPreference:(NSString *)key
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    return [pref boolForKey:key];
}
@end
