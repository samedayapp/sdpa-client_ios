//
//  SignInVC.m
//  SDPAClient
//
//  Created by Sapana Ranipa on 24/12/15.
//  Copyright © 2015 Sapana Ranipa. All rights reserved.
//

#import "SignInVC.h"

@interface SignInVC ()
{
    NSString *strForStripeToken,*strForLastFour;
}
@end

@implementation SignInVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self CheckLogin];

    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.viewForAddCard.hidden=YES;
    self.ViewForForgetPassword.hidden=YES;
    [self.navigationController setNavigationBarHidden:YES];
    [self SetLocalization];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [self.view endEditing:YES];
    self.view.frame=CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
}
-(void)CheckLogin
{
    if([NSPref GetBoolPreference:PREF_IS_LOGIN])
    {
        NSString *str=[NSPref GetPreference:PREF_TOKEN];
        if(str.length>0)
        {
            [self performSegueWithIdentifier:LOGIN_SUCCESS sender:self];
        }
    }
}
#pragma mark -
#pragma mark - Localization Methods
-(void)SetLocalization
{
    // Set Localization
    self.lblCopyRights.text=NSLocalizedString(@"COPY_RIGHTS", nil);
    self.lblHaveAccount.text=NSLocalizedString(@"ACCOUNT_YET", nil);
    self.lblNoWorries.text=NSLocalizedString(@"NO_WORRIES",nil);
    self.lblMailInfo.text=NSLocalizedString(@"EMAIL_INFO",nil);
    
    self.txtUserName.placeholder=NSLocalizedString(@"USERNAME", nil);
    self.txtPassword.placeholder=NSLocalizedString(@"PASSWORD", nil);
    self.txtForgetPassword.placeholder=NSLocalizedString(@"EMAIL_ID", nil);
    
    self.txtCardNumber.placeholder=NSLocalizedString(@"CREDIT_CARD_NUMBER", nil);
    self.txtMonth.placeholder=NSLocalizedString(@"MM", nil);
    self.txtYear.placeholder=NSLocalizedString(@"YY", nil);
    self.txtCVC.placeholder=NSLocalizedString(@"CVV", nil);
    self.lblCardinfo.text=NSLocalizedString(@"CARD_INFO_MSG", nil);
    
    [self.BtnAddCard setTitle:NSLocalizedString(@"ADD_CARD", nil) forState:UIControlStateNormal];
    [self.BtnAddCard setTitle:NSLocalizedString(@"ADD_CARD", nil) forState:UIControlStateSelected];
    [self.btnBack setTitle:NSLocalizedString(@"BTN_BACK", nil) forState:UIControlStateNormal];
    [self.btnBack setTitle:NSLocalizedString(@"BTN_BACK", nil) forState:UIControlStateSelected];
    [self.btnForgetPassword setTitle:NSLocalizedString(@"FORGET_PASSWORD", nil) forState:UIControlStateNormal];
    [self.btnForgetPassword setTitle:NSLocalizedString(@"FORGET_PASSWORD", nil) forState:UIControlStateSelected];
    [self.btnLogin setTitle:NSLocalizedString(@"LOGIN", nil) forState:UIControlStateNormal];
    [self.btnLogin setTitle:NSLocalizedString(@"LOGIN", nil) forState:UIControlStateSelected];
    [self.btnRegisterHere setTitle:NSLocalizedString(@"REGISTER_HERE", nil) forState:UIControlStateNormal];
    [self.btnRegisterHere setTitle:NSLocalizedString(@"REGISTER_HERE", nil) forState:UIControlStateSelected];
    [self.btnCloseForForgetPasswordView setTitle:NSLocalizedString(@"CLOSE", nil) forState:UIControlStateNormal];
    [self.btnCloseForForgetPasswordView setTitle:NSLocalizedString(@"CLOSE", nil) forState:UIControlStateSelected];
    [self.btnForgetPasswordSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
    [self.btnForgetPasswordSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateSelected];
    
    
    // Set color
    [self.txtUserName setValue:[UIColor colorWithRed:0.0/255.0 green:84.0/255.0 blue:107.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtPassword setValue:[UIColor colorWithRed:0.0/255.0 green:84.0/255.0 blue:107.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtForgetPassword setValue:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtCardNumber setValue:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtMonth setValue:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtYear setValue:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
    [self.txtCVC setValue:[UIColor colorWithRed:159.0/255.0 green:162.0/255.0 blue:164.0/255.0 alpha:1] forKeyPath:@"_placeholderLabel.textColor"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - UIButton Action Methods

- (IBAction)onClickBtnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onClickBtnLogin:(id)sender
{
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.5 animations:^{
        
        self.view.frame=CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
        
    } completion:^(BOOL finished)
     {
     }];
    if([[AppDelegate sharedAppDelegate] connected])
    {
        if(self.txtUserName.text.length<1 || self.txtPassword.text.length<1)
        {
            if(self.txtUserName.text.length<1)
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_ENTER_USERNAME", nil)];
            }
            /*else if (![[UtilityClass sharedObject]isValidEmailAddress:self.txtUserName.text])
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_VALID_EMAIL", nil)];
            }*/
            else if (self.txtPassword.text.length<1)
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_ENTER_PASSWORD", nil)];
            }
        }
        else
        {
            [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"LOGINING", nil)];
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
            [dictParam setObject:device_token forKey:PARAM_DEVICE_TOKEN];
            [dictParam setObject:device_type forKey:PARAM_DEVICE_TYPE];
            [dictParam setObject:self.txtUserName.text forKey:PARAM_USERNAME];
            [dictParam setObject:self.txtPassword.text forKey:PARAM_PASSWORD];
            
            AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:P_LOGIN withParamData:dictParam withBlock:^(id response, NSError *error)
            {
                if(response)
                {
                    response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                    [[AppDelegate sharedAppDelegate] hideLoadingView];
                    if([[response valueForKey:@"success"] boolValue])
                    {
                        [NSPref SetPreference:PREF_TOKEN Value:[response valueForKey:@"token"]];
                        [NSPref SetPreference:PREF_ID Value:[response valueForKey:@"id"]];
                        if([[response valueForKey:@"card_available"] boolValue])
                        {
                            NSLog(@"Login response ------> %@",response);
                            [APPDELEGATE showToastMessage:NSLocalizedString(@"SIGING_SUCCESS", nil)];
                            [NSPref SetBoolPreference:PREF_IS_LOGIN Value:YES];
                            [NSPref SetPreference:PREF_LOGIN_OBJECT Value:response];
                            [self performSegueWithIdentifier:LOGIN_SUCCESS sender:self];
                        }
                        else
                        {
                            self.viewForAddCard.hidden=NO;
                            self.btnBack.hidden=YES;
                        }
                    }
                    else
                    {
                        NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                        [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                    }
                }
            }];
        }
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }

}
- (IBAction)OnClickBtnForgetPasswordSubmit:(id)sender
{
    [self.view endEditing:YES];
    if([[AppDelegate sharedAppDelegate] connected])
    {
        if(self.txtForgetPassword.text.length<1 || ![[UtilityClass sharedObject]isValidEmailAddress:self.txtForgetPassword.text])
        {
            if(self.txtForgetPassword.text.length<1)
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_ENTER_EMAIL", nil)];
            }
            else if (![[UtilityClass sharedObject]isValidEmailAddress:self.txtForgetPassword.text])
            {
                [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_VALID_EMAIL", nil)];
            }
        }
        else
        {
            [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"SENDING_MAIL", nil)];
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
            [dictParam setObject:self.txtForgetPassword.text forKey:PARAM_USERNAME];
            [dictParam setObject:@"2" forKey:PARAM_TYPE];
            
            AFNHelper *afn=[[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:P_FORGET_PASSWORD withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 if(response)
                 {
                     response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                     [[AppDelegate sharedAppDelegate]hideLoadingView];
                        if([[response valueForKey:@"success"] boolValue])
                        {
                            NSLog(@"ForgetPassword response ------> %@",response);
                            self.ViewForForgetPassword.hidden=YES;
                            [APPDELEGATE showToastMessage:NSLocalizedString(@"PASSWORD_SENT_SUCCESS", nil)];
                        }
                        else
                        {
                            NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                            [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:NSLocalizedString(str, nil)];
                        }
                 }
             }];
        }
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }

}
- (IBAction)onClickForgetPassword:(id)sender
{
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.5 animations:^{
        
        self.view.frame=CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
        
    } completion:^(BOOL finished)
     {
     }];
    self.txtForgetPassword.text=@"";
    self.ViewForForgetPassword.hidden=NO;
}

- (IBAction)OnClickBtnCloseForForgetPasswordView:(id)sender
{
    [self.view endEditing:YES];
    self.ViewForForgetPassword.hidden=YES;
}
- (IBAction)onClickBtnAddCard:(id)sender
{
    if(self.txtCardNumber.text.length<1 || self.txtMonth.text.length<1 || self.txtYear.text.length<1 || self.txtCVC.text.length<1)
    {
        if(self.txtCardNumber.text.length<1)
        {
            [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_CREDIT_CARD_NUMBER", nil)];
        }
        else if(self.txtMonth.text.length<1)
        {
            [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_MONTH", nil)];
        }
        else if(self.txtYear.text.length<1)
        {
            [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLEASE_YEAR", nil)];
        }
        else if(self.txtCVC.text.length<1)
        {
            [[UtilityClass sharedObject]showAlertWithTitle:@"" andMessage:NSLocalizedString(@"PLESE_CVV", nil)];
        }
    }
    else
    {
        if (![Stripe defaultPublishableKey])
        {
            [[UtilityClass sharedObject] showAlertWithTitle:NSLocalizedString(@"NO_PUBLISH_KEY", nil) andMessage:NSLocalizedString(@"PUBLIS_MESSAGE", nil)];
            
            return;
        }
        [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"ADDING_CARD", nil)];
        STPCard *card = [[STPCard alloc] init];
        
        NSString *strCard=[self.txtCardNumber.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        card.number =strCard;
        card.expMonth =[self.txtMonth.text integerValue];
        card.expYear = [self.txtYear.text integerValue];
        card.cvc = self.txtCVC.text;
        
        [Stripe createTokenWithCard:card completion:^(STPToken *token, NSError *error) {
            if (error)
            {
                [[AppDelegate sharedAppDelegate] hideLoadingView];
                [self hasError:error];
            }
            else
            {
                [self hasToken:token];
                [self addCardOnServer];
            }
        }];
        
    }
}
- (void)hasError:(NSError *)error
{
    [[UtilityClass sharedObject] showAlertWithTitle:NSLocalizedString(@"Error", @"Error") andMessage:[error localizedDescription]];
    [APPDELEGATE hideLoadingView];
}

- (void)hasToken:(STPToken *)token
{
    strForLastFour=token.card.last4;
    strForStripeToken=token.tokenId;
}
-(void)addCardOnServer
{
    if([[AppDelegate sharedAppDelegate] connected])
    {
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:[NSPref GetPreference:PREF_ID] forKey:PARAM_ID];
        [dictParam setObject:[NSPref GetPreference:PREF_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setObject:strForStripeToken forKey:PARAM_STRIPE_TOKEN];
        [dictParam setObject:strForLastFour forKey:PARAM_LAST_FOUR];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:P_ADD_CARD withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             [APPDELEGATE hideLoadingView];
             if(response)
             {
                 response = [[UtilityClass sharedObject]dictionaryByReplacingNullsWithStrings:response];
                 [[AppDelegate sharedAppDelegate] hideLoadingView];
                 if ([[response valueForKey:@"success"] boolValue])
                 {
                     [APPDELEGATE showToastMessage:NSLocalizedString(@"ADD_CARD_SUCCESS", nil)];
                     self.txtCardNumber.text=@"";
                     self.txtMonth.text=@"";
                     self.txtYear.text=@"";
                     self.txtCVC.text=@"";
                     self.viewForAddCard.hidden=YES;
                     self.btnBack.hidden=NO;
                     [NSPref SetBoolPreference:PREF_DEFAULT_CARD Value:@"1"];
                    // [self performSegueWithIdentifier:LOGIN_SUCCESS sender:self];
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error_code"]];
                     if([str isEqualToString:@"21"])
                     {
                         [self performSegueWithIdentifier:SEGUE_TO_UNWIND sender:self];
                     }
                     else
                     {
                         [[UtilityClass sharedObject] showAlertWithTitle:@"" andMessage:[response valueForKey:@"strip_msg"]];
                     }
                 }
             }
         }];
    }
    else
    {
        [[UtilityClass sharedObject]showAlertWithTitle:NSLocalizedString(@"NETWORK_STATUS", nil) andMessage:NSLocalizedString(@"NO_INTERNET", nil)];
    }
    
}

#pragma mark -
#pragma mark - UITextField Delegate Methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.txtCardNumber)
    {
        if (string==nil || [string isEqualToString:@""] || [string isKindOfClass:[NSNull class]] || string.length < 1)
        {
            
        }
        else if(self.txtCardNumber.text.length==4 || self.txtCardNumber.text.length==9 || self.txtCardNumber.text.length==14)
        {
            NSString *str=self.txtCardNumber.text;
            self.txtCardNumber.text=[NSString stringWithFormat:@"%@ ",str];
        }
        
        
        if (self.txtCardNumber.text.length == Card_Length && range.length == 0)
        {
            if(self.txtMonth.text.length >= Card_Month)
            {
                return NO;
            }
            else
            {
                [self.txtMonth becomeFirstResponder];
            }
        }
    }
    else if (textField == self.txtMonth)
    {
        if (self.txtMonth.text.length >= Card_Month && range.length == 0)
        {
            if(self.txtYear.text.length >= Card_Year)
            {
                return NO;
            }
            else
            {
                [self.txtYear becomeFirstResponder];
            }
        }
        
    }
    else if (textField == self.txtYear)
    {
        if (self.txtYear.text.length >= Card_Year && range.length == 0)
        {
            if(self.txtCVC.text.length >= Card_CVC_CVV)
            {
                return NO;
            }
            else
            {
                [self.txtCVC becomeFirstResponder];
            }
        }
    }
    else
    {
        if (self.txtCVC.text.length >= Card_CVC_CVV && range.length == 0)
        {
            [self.txtCVC resignFirstResponder];
            [UIView animateWithDuration:0.5 animations:^{
                
                self.viewForAddCard.frame=CGRectMake(self.viewForAddCard.frame.origin.x,65, self.viewForAddCard.frame.size.width, self.viewForAddCard.frame.size.height);
                
            } completion:^(BOOL finished)
             {
             }];
        }
    }
    
    return YES;
}
/*
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    int y=0;
    if(textField==self.txtUserName)
    {
        y=-100;
    }
    else if(textField==self.txtPassword)
    {
        y=-150;
    }
    [UIView animateWithDuration:0.5 animations:^{
        
        self.view.frame=CGRectMake(self.view.frame.origin.x, y, self.view.frame.size.width, self.view.frame.size.height);
        
    } completion:^(BOOL finished)
     {
     }];
    if(textField==self.txtForgetPassword)
    {
        [UIView animateWithDuration:0.5 animations:^{
            
            self.ViewForForgetPassword.frame=CGRectMake(self.ViewForForgetPassword.frame.origin.x, -150, self.ViewForForgetPassword.frame.size.width, self.ViewForForgetPassword.frame.size.height);
            
        } completion:^(BOOL finished)
         {
         }];
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==self.txtUserName)
    {
        [self.txtPassword becomeFirstResponder];
    }
    else if(textField==self.txtPassword)
    {
        [self.txtPassword resignFirstResponder];
        [UIView animateWithDuration:0.5 animations:^{
            
            self.view.frame=CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
            
        } completion:^(BOOL finished)
         {
         }];
    }
    else if(textField==self.txtForgetPassword)
    {
        [self.txtForgetPassword resignFirstResponder];
        [UIView animateWithDuration:0.5 animations:^{
            
            self.ViewForForgetPassword.frame=CGRectMake(self.ViewForForgetPassword.frame.origin.x,0, self.ViewForForgetPassword.frame.size.width, self.ViewForForgetPassword.frame.size.height);
            
        } completion:^(BOOL finished)
         {
         }];
    }
    return YES;
}
*/
#pragma mark -
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}


@end
