//
//  BidCell.h
//  SDPAClient
//
//  Created by Sapana Ranipa on 11/01/16.
//  Copyright © 2016 Sapana Ranipa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BidCell : UITableViewCell
{

}
@property (weak, nonatomic) IBOutlet UIImageView *img_profile;
@property (weak, nonatomic) IBOutlet UILabel *lbl_name;
@property (weak, nonatomic) IBOutlet UILabel *lbl_time;
@property (weak, nonatomic) IBOutlet UILabel *lbl_remain_time;
@property (weak, nonatomic) IBOutlet UILabel *lbl_rate;
@property (weak, nonatomic) IBOutlet UILabel *lbl_price;
@property (weak, nonatomic) IBOutlet UILabel *lblDeliveryDate;

@property (weak, nonatomic) IBOutlet UIButton *btn_Accept;
@property (weak, nonatomic) IBOutlet UIButton *btn_Reject;
@property (weak, nonatomic) IBOutlet UIButton *btn_viewNote;
@property (weak, nonatomic) IBOutlet UIView *viewForRate;
@property (weak, nonatomic) IBOutlet UITextView *textViewForNote;

@end

