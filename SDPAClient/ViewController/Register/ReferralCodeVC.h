//
//  ReferralCodeVC.h
//  SDPAClient
//
//  Created by Sapana Ranipa on 28/12/15.
//  Copyright © 2015 Sapana Ranipa. All rights reserved.
//

#import "BaseVC.h"

@interface ReferralCodeVC : BaseVC <UITextFieldDelegate>
{

}
@property (weak, nonatomic) IBOutlet UITextView *textMessage;
@property (weak, nonatomic) IBOutlet UITextField *txtCode;

@property (weak, nonatomic) IBOutlet UIButton *btnSkip;
@property (weak, nonatomic) IBOutlet UIButton *btnAdd;

- (IBAction)onClickBtnSkip:(id)sender;
- (IBAction)onClickBtnAdd:(id)sender;
@end
