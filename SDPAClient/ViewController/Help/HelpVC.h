//
//  HelpVC.h
//  SDPAClient
//
//  Created by Sapana Ranipa on 01/01/16.
//  Copyright © 2016 Sapana Ranipa. All rights reserved.
//

#import "BaseVC.h"

@interface HelpVC : BaseVC<UIWebViewDelegate>
{

}

@property (weak, nonatomic) IBOutlet UIWebView *webObj;

@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
- (IBAction)onClickBtnMenu:(id)sender;

@end
