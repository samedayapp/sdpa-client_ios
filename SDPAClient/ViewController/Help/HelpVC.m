//
//  HelpVC.m
//  SDPAClient
//
//  Created by Sapana Ranipa on 01/01/16.
//  Copyright © 2016 Sapana Ranipa. All rights reserved.
//

#import "HelpVC.h"

@interface HelpVC ()

@end

@implementation HelpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self SetLocalization];
    [self.navigationController setNavigationBarHidden:NO];
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
    self.navigationItem.hidesBackButton=YES;
    NSURL *websiteUrl = [NSURL URLWithString:HELP_URL];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:websiteUrl];
    [self.webObj loadRequest:urlRequest];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [APPDELEGATE hideLoadingView];
}
#pragma mark -
#pragma mark - Localization Methods
-(void)SetLocalization
{
    [self.btnMenu setTitle:NSLocalizedString(@"HELP",nil) forState:UIControlStateNormal];
    [self.btnMenu setTitle:NSLocalizedString(@"HELP",nil) forState:UIControlStateSelected];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark -
#pragma mark - UIbutton Action Methods
- (IBAction)onClickBtnMenu:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
